import pygame#255,130,200 For perfect pink

import menu
bx = 20
by = 10
y_add = 80
char_rect = pygame.Rect(bx,by,400,y_add-10)
class char(object):
	def __init__(self, name, speed, ap, colour):
		self.name = name
		self.ap = int(ap)
		self.speed = int(speed)
		
		content = []
		self.ap_label = menu.label([5,25],str(self.ap),"Current Points:")
		self.spd_label = menu.label([5,50],str(self.speed),"Gain per turn:")
		self.rank_label = menu.label([360,1],"NaN","#")
		
		r = pygame.Rect(160,30,40,15)
		self.ap_change = menu.input_box(r,"Major AP loss","100")
		r = pygame.Rect(160,50,40,15)
		self.act_button = menu.button( r, "ACT", ["ONETIME",self.act,self.ap_change] )
		
		r = pygame.Rect(230,30,40,15)
		self.secondary_change = menu.input_box(r,"Minor AP loss","20")
		r = pygame.Rect(230,50,40,15)
		self.s_act_button = menu.button( r, "ACT", ["ONETIME",self.act,self.secondary_change] )

		r = pygame.Rect(300,30,40,15)
		self.custom_change = menu.input_box(r,"Custom AP loss","0")
		r = pygame.Rect(300,50,40,15)
		self.c_act_button = menu.button( r, "ACT", ["ONETIME",self.act,self.custom_change] )
		
		content.append(self.ap_label)
		content.append(self.spd_label)
		content.append(self.rank_label)
		content.append(self.ap_change)
		content.append(self.act_button)
		content.append(self.secondary_change)
		content.append(self.s_act_button)
		content.append(self.custom_change)
		content.append(self.c_act_button)
		from game import PROGRAM
		os = len(PROGRAM.chars)
		rect = char_rect.copy()
		self.base = menu.menu_box(rect,content,colour, "Name: " + self.name, )
		PROGRAM.GUI.append(self.base)
		
	def pass_turn(self):
		self.ap += self.speed
		
	def update_sheet(self,pos,num):
		self.ap_label.change_value(self.ap)
		self.rank_label.change_value(num)
	#	self.base.rect.move_ip( 0 , y_add * pos )
		self.base.rect.topleft = ( bx, by + y_add*pos )
		self.base.set_positions()
		
	def act(self,field):
		try:
			loss = int(field.string)
		except ValueError:
			print field.context,"Should contain a number"
			return 
		if self.ap >= loss:
			self.ap -= loss
			from game import PROGRAM
			PROGRAM.sort_chars()
			PROGRAM.refresh_GUI()
		else:
			print"not enough Point XD:DD"
		