This is simple program to keep count of turns in home-brew version of Pen&Paper game Spirit of the Century

Character parameters are entered into terminal once "Add Character" button is pressed. ( please note that graphical window is prone to crashing during this time )

As optional parameters you can enter colour for character along side the name (Example : "Stanley Birch,50,80,160" input would set (50,80,160) as RGB colour of Stanley Birch)
Also, you can set the amount of Action Points character starts out with when setting his speed (Example (8,150) would make character start out with 150 AP instead of 0)


Each time pass turn button is pressed each chars AP is increased by their speed. Char with most AP is kept at the top of the list.

Each char has three actions which can be set to take desired amount of AP away from him. pressing ACT button under then input box will take that amount of points away from the charachter


There is also option to edit the base AP gain of character or the current amount of AP. Input here is done also via Terminal.