import pygame
from pygame.locals import *
import random


import menu
import char



class game():
	
	def __init__(self):
		
		self.FPS = 30 # 60 frames per second
		
		#self.FPS = 6 # 60 frames per second
		
		self.clock = pygame.time.Clock()
		SWIDTH =  1000
		SHEIGTH = 600	
		self.MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))
		pygame.display.set_caption("Turncounter for NERDS ")

		self.mouse = [pygame.mouse.get_pos() , False ,  [0,0] , None]
		self.game_keymap = {}
		
		self.active_text_field = None
		self.surf_GUI = pygame.Surface((SWIDTH, SHEIGTH))
		self.updates = []
		self.erase = []
		self.draw = []
		#path = os.path.join("recourses","cursor.bmp")
		
		self.funktion = None
		self.GUI = []
		self.done = False
		self.debug_message = ""
	
	def m_loop(self):
		while not self.done:
			
			#self.keys = [self.keys,pygame.key.get_pressed()] , key=lambda candidate: vector.simple_distance(candidate.pos,char.tile.pos))
			self.keys = []
			self.mouse[0] = pygame.mouse.get_pos()
			self.mouse[1] = False
			for event in pygame.event.get(): # User did something
				if event.type == pygame.QUIT: # Closed from X
					self.done = True # Stop the Loop
				if event.type == pygame.MOUSEBUTTONUP:
					self.mouse[1] = [True]
					for object in self.GUI:#check if any in game buttons are pressed
						if object.pressed(self.mouse[0]):
							object.act()
				if event.type == pygame.KEYDOWN:
					self.keys.append(event.key)
					try:
						self.run_command(self.game_keymap[event.key])
					except KeyError:
						pass
					
			#print len(self.keys), self.active_text_field
			if len(self.keys) > 0 and not self.active_text_field == None:
				self.active_text_field.update()
						#key pressed was mapped for nothing
			self.funktion()
			pygame.display.update(self.updates)			
			self.updates = []
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
	def scounter(self):
		self.GUI = []
		self.chars = []
		buttons = []
		self.scroll_position = 0
		self.max_scroll_position = 1
		mx = 200
		my = self.MainWindow.get_size()[1]-20
		#pos = (self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-100)
		pos = (20,20)
		#self.GUI.append(menu.button(pygame.Rect(pos,(160,80)),"Next round",[self.pass_round]))
		buttons.append(menu.button(pygame.Rect(pos,(160,80)),"Next round",["ONETIME",self.pass_round]))
		#pos = (self.MainWindow.get_size()[0]-180,self.MainWindow.get_size()[1]-200)
		pos = (20,120)
		buttons.append(menu.button(pygame.Rect(pos,(160,80)),"Add Charachter",["ONETIME",self.add_char]))
		pos = (20,220)
		buttons.append(menu.button(pygame.Rect(pos,(160,80)),"Change Speed",["ONETIME",self.change_speed]))		
		pos = (20,320)
		buttons.append(menu.button(pygame.Rect(pos,(160,80)),"Change Points",["ONETIME",self.change_ap]))				
		#scroll buttons
		pos = (20,my-60)
		buttons.append(menu.button(pygame.Rect(pos,(160,25)),"^",["ONETIME",self.scroll,1]))
		pos = (20,my-30)
		buttons.append(menu.button(pygame.Rect(pos,(160,25)),"v",["ONETIME",self.scroll,-1]))
		
		#pos = (self.MainWindow.get_size()[0]-220,20,200,self.MainWindow.get_size()[0]-20)
		r = pygame.Rect(self.MainWindow.get_size()[0]-210,10,mx,my)
		self.GUI.append(menu.menu_box(r,buttons,(255,165,75),title = "Testi:3"))
				
		
		self.MainWindow.fill((100,100,100))
		self.refresh_GUI()
		pygame.display.flip()
		self.funktion = self.counter
		
	def counter(self):
		pass
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~pasdasdasdasd~~~~~~~~~~~~~~~~~~~~~~~~
	def add_char(self):
		ap = 0
		c = [35,145,255]
		name = ""
		speed = 0
		while (name == ""):
			pname = raw_input("Name,(colour): ")
			try:
				n = pname.split(",")
			except AttributeError:
				n = ""
			if len(n) > 1:
				if len(n[0]) > 0:
					name = n[0]
				else:
					print "Not a proper name"
				try:
					r,g,b = n[1:]
					c = [int(r),int(g),int(b)]
				except ValueError:
					print "Not valid colour value"
					
		while (not speed > 0):
			pspeed = raw_input("Speed,(initial points): ")
			s = pspeed.split(",")
			try: 
				speed = int(s[0])
			except ValueError:
				print "not proper speed value"
			if len(s) > 1:
				try:
					ap = int(s[1])
				except ValueError:
					pass
				
		nc = char.char(name,speed,ap,c)
		self.chars.append(nc)
		self.sort_chars()
		self.refresh_GUI()
		self.max_scroll_position = 1-len(self.chars)
	def draw_chars(self):
		pass
	
	def pass_round(self):
		for c in self.chars:
			c.pass_turn()
		self.sort_chars()
		self.refresh_GUI()
			
	def sort_chars(self):
		self.chars = sorted(self.chars, key=lambda c: -c.ap)
		self.move_sheets()
	def move_sheets(self):
		x = self.scroll_position
		n = 1
		for c in self.chars:
			c.update_sheet(x,n)
			x += 1 
			n += 1
		
	def run_command(self, commands):
		for c in commands:
			command = c[:]
			if command[0] == "ONETIME":
				command.pop(0)
				f = command[0]
				command.pop(0)
				f(*command)
			else:
				self.funktion = command[0]
				
	def refresh_GUI(self):
		for object in self.GUI:
			object.draw()
			
	def add_to_(self,object,list):
		list.append(object)
		
	def scroll(self,change):
		new = self.scroll_position + change
		print "new",new,"max",self.max_scroll_position
		if new <= 0 and new > self.max_scroll_position:
			self.scroll_position = new
			self.sort_chars()
			self.MainWindow.fill((100,100,100))
			self.refresh_GUI()
			pygame.display.flip()
			
	def change_speed(self):
		name = raw_input("Who you wish to edit? ")
		on = name
		name = name.upper()
		f = False
		for c in self.chars:
			if c.name.upper() == name:
				f = True
				spd = raw_input("new speed for "+c.name+"? ")
				try:
					spd = int(spd)
					c.speed = spd
					c.spd_label.change_value(spd)
					self.refresh_GUI()
					print "done"
					break
				except ValueError:
					print "input is not a number, keeping old value"
				break
		if not f:
			print "couldn't find character named",on
			
	def change_ap(self):
		name = raw_input("Who you wish to edit? ")
		on = name
		name = name.upper()
		f = False
		for c in self.chars:
			if c.name.upper() == name:
				f = True
				ap = raw_input("new AP amount for "+c.name+"? ")
				try:
					ap = int(ap)
					c.ap = ap
					c.ap_label.change_value(ap)
					self.sort_chars()
					self.refresh_GUI()
					print "done"
					break
				except ValueError:
					print "input is not a number, keeping old value"
				break
		if not f:
			print "couldn't find character named",on			
def start():
	global PROGRAM
	PROGRAM = game()
	PROGRAM.funktion = PROGRAM.scounter
	PROGRAM.m_loop()
	pygame.quit()		
